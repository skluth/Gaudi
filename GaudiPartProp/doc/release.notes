!-----------------------------------------------------------------------------
! Package     : GaudiPartProp
! Responsible : Marco Clemencic
! Purpose     : Gaudi Particle Property Service
! Commit Id   : $Format:%H$
!-----------------------------------------------------------------------------

============================= GaudiPartProp v2r3 =============================

! 2016-03-10 - commit 4a18175

 - removed uses of templates implements[1-4], extends[1-4] and
   extend_interfaces[1-4]

   Since merge request !22 they are unnecessary.
    See merge request !133

! 2016-02-11 - commit b0618f7

 - Improve CommonMessaging

   Implementation of GAUDI-1146: Improve CommonMessaging and use it in more
   places.

   See merge request !76

   Conflicts:
   GaudiExamples/tests/qmtest/refs/AlgTools2.ref
   GaudiExamples/tests/qmtest/refs/MultiInput/Read.ref
   GaudiExamples/tests/qmtest/refs/conditional_output/write.ref
   GaudiKernel/GaudiKernel/AlgTool.h
   GaudiKernel/GaudiKernel/Algorithm.h
   GaudiKernel/GaudiKernel/Service.h
   GaudiKernel/src/Lib/AlgTool.cpp
   GaudiKernel/src/Lib/Algorithm.cpp

============================= GaudiPartProp v2r2 =============================

! 2015-10-08 - commit c50c176

 - improvements to documentation and release tools

   See merge request !43


! 2015-09-25 - commit 35dd00c

 - Merge branch 'dev-smartif-use' into 'master'

   Provide (and use) C++11 smart pointer 'look and feel' for SmartIF

   The aim of this branch is to confine, for everything that inherits from
   IInterface, the calls to addRef(), release() and queryInterface() to the
   SmartIF implementation. Exceptions are a few places where interfaces
   (currently) return bare pointers (instead of SmartIF...) and where one thus
   has to addRef() explicitly to avoid returning a dangling pointer. This can be
   avoided by changing the relevant interface to return a SmartIF instead of a
   bare pointer.

   In addition, make SmartIF 'look and feel' like a smart pointer.

   - use explict bool conversion instead of .isValid()
   - add SmartIF::as<IFace>(), to return a SmartIF<IFace> to an alternate
      interface -- which (together with move) encourages the use of auto
   - add ISvcLocator::as<IFace>(), to return a SmartIF<IFace> to the current
   ISvcLocator.
   - add ServiceManager::service<IFace>() which return SmartIF<IFace> which
   encourages
      the use of auto

   And add a few other C++11 modernizations (eg. prefer STL over raw loop)

   Fixes GAUDI-1094

   See merge request !24


! 2015-09-11 - commit c062cbe

 - C++11 modernization changes

   Some trivial - and some not so trivial! - changes which take advantage of
   C++11...

   See merge request !7

============================= GaudiPartProp v2r1 =============================
! 2014-03-18 - Ben Couturier
 - Fixed bug #104127: remove hwaf configuration.

============================= GaudiPartProp v2r0 =============================
! 2013-12-11 - Sebastien Binet
 - Added hwaf configuration files.

! 2013-07-18 - Marco Clemencic
 - Removed use of obsolete AlgFactory.h, SvcFactory.h, ToolFactory.h,
   AudFactory.h and CnvFactory.h.

! 2013-07-17 - Marco Clemencic
 - Removed factories forward declarations.

============================= GaudiPartProp v1r2 =============================
! 2012-11-14 - Marco Clemencic
 - Updated CMakeLists.txt.

============================= GaudiPartProp v1r1 =============================
! 2012-06-08 - Marco Clemencic
 - Added CMake configuration files.

============================= GaudiPartProp v1r0 =============================
! 2011-12-05 - Marco Clemencic
 - Moved ParticlePropertySvc to a dedicated package and wrapped it in the
   namespace Gaudi.
