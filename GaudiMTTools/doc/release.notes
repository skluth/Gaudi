!-----------------------------------------------------------------------------
! Package     : GaudiMTTools
! Responsible : Illya SHAPOVAL
! Purpose     : The package for Gaudi multithreading tools and machinery
!-----------------------------------------------------------------------------

============================= GaudiMTTools v1r2 ==============================

! 2016-02-11 - commit e2c585c

 - Remove globalTimeOffset getter and GlobalTimeOffset property from GaudiCommon

   GaudiCommon adds a property GlobalTimeOffset and a corresponding getter
   globalTimeOffset to every algorithm/tool which uses GaudiCommon, regardless
   on whether this property is used.

   As there is no code anymore that actually uses globalTimeOffset, this patch
   removes this accessor and the corresponding property.

   Fixes GAUDI-1122.

   See merge request !61

============================= GaudiMTTools v1r1 ==============================

! 2016-01-12 - commit 4dbdc8e

 - fixed clang 3.7 compilation warnings

   Fixes GAUDI-1083.
 See merge request !89

! 2015-11-02 - commit 57f356c

 - Merge branch 'hive' into 'master'

   Fixes GAUDI-978.

   See merge request !65

!======================= GaudiMTTools v1r0 2012-01-27 ============================
! 2012-01-27 - Illya Shapoval
 - Requirements file is fixed: explicit Boost dependecy and linking option is added.

! 2012-01-26 - Illya Shapoval
 - GaudiParallelizer prototype is created. This is an algorithm manager similar
   to GaudiSequencer with the difference in that it runs algorithms in parallel
   for each event. Algorithms being executed in parallel have to be independent
   at this moment (i.e. no input/output data dependencies). Internally
   GaudiParallelizer uses Threadpool 0.2.5 library based on Boost to manage
   thread and task pools.
