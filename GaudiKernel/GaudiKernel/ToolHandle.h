#ifndef GAUDIKERNEL_TOOLHANDLE_H
#define GAUDIKERNEL_TOOLHANDLE_H

//Includes
#include "GaudiKernel/GaudiHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/INamedInterface.h"
#include "GaudiKernel/IAlgTool.h"

#include <string>
#include <vector>
#include <stdexcept>
#include <type_traits>

// forward declarations
class IInterface;
class IToolSvc;

class Algorithm;
class AlgTool;
class Service;

/** General info and helper functions for toolhandles and arrays */
class ToolHandleInfo {
protected:
  ToolHandleInfo(const IInterface* parent = nullptr, bool createIf = true )
    : m_parent(parent), m_createIf(createIf)
  {}

public:
  virtual ~ToolHandleInfo() = default;

  bool isPublic() const { return !m_parent; }

  bool createIf() const { return m_createIf; }

  const IInterface* parent() const { return m_parent; }

  //
  // Some helper functions
  //
  static std::string toolComponentType(const IInterface* parent) {
    return parent ? "PrivateTool" : "PublicTool";
  }

  static std::string toolParentName(const IInterface* parent) {
    if (!parent) return "ToolSvc";
    const INamedInterface* pNamed = dynamic_cast<const INamedInterface*>(parent);
    return pNamed ? pNamed->name() : "";
  }

protected:
  const IInterface* m_parent;
  bool m_createIf;

};

/** @class BaseToolHandle ToolHandle.h GaudiKernel/ToolHandle.h

    Non-templated base class for actual ToolHandle<T>.
    Defines the interface for the management of ToolHandles in the Algorithms and Tools

    @author Daniel Funke <daniel.funke@cern.ch>
*/
class BaseToolHandle: public ToolHandleInfo {

protected:
  BaseToolHandle(const IInterface* parent = nullptr, bool createIf = true )
    : ToolHandleInfo(parent, createIf)
  {}

  virtual StatusCode i_retrieve(IAlgTool*&) const = 0;

public:
  virtual ~BaseToolHandle() {}

  StatusCode retrieve(IAlgTool*& tool) const {
    return i_retrieve(tool);
  }

  virtual IAlgTool* get() const = 0;
  virtual std::string typeAndName() const = 0;
};

/** @class ToolHandle ToolHandle.h GaudiKernel/ToolHandle.h

    Handle to be used in lieu of naked pointers to tools. This allows
    better control through the framework of tool loading and usage.
    @paramater T is the AlgTool interface class (or concrete class) of
    the tool to use, and must derive from IAlgTool.

    @author Wim Lavrijsen <WLavrijsen@lbl.gov>
    @author Martin.Woudstra@cern.ch
*/
template< class T >
class ToolHandle : public BaseToolHandle, public GaudiHandle<T> {

  friend class Algorithm;
  friend class AlgTool;
  friend class Service;

public:
  /** Constructor for a tool with default tool type and name.
      Can be called only if the type T is a concrete tool type (not an interface),
      and you want to use the default name. */
  ToolHandle(const IInterface* parent = nullptr, bool createIf = true)
    : BaseToolHandle(parent,createIf),
      GaudiHandle<T>( GaudiHandle<T>::getDefaultType(),
                      toolComponentType(parent),
                      toolParentName(parent) ),
      m_pToolSvc( "ToolSvc", GaudiHandleBase::parentName() )
  {  }

 public:
  //
  // Constructors etc.
  //

  /** Create a handle ('smart pointer') to a tool.
      The arguments are passed on to ToolSvc, and have the same meaning:
      @code
      StatusCode ToolSvc::retrieveTool ( const std::string& type            ,
                                         T*&                tool            ,
                                         const IInterface*  parent   = 0    ,
                                         bool               createIf = true )
      @endcode
      @param owner: class owning the ToolHandle
      @param toolType: "MyToolType/MyToolName"
                       "MyToolType" is short for "MyToolType/MyToolType"
                       'MyToolType' is the name of the class of the concrete tool
                       'MyToolName' is to distinguish several tool instances of the same class
      @param parent: the parent Algorithm,Tool or Service of which this tool is a member.
                     If non-zero, the the tool is a private tool of the parent, otherwise it is
                     a public (shared) tool.
      @param createIf: if true, create tool if not yet existing.
  */

#if defined(TOOLHANDLE_DEPR_WARN)
  //warn about using deprecated explicit ToolHandle construction
#pragma message("Untracked ToolHandle: Migrate explicit DataHandle constructor to declareTool Algorithm Property")

  __attribute__ ((deprecated))

#endif
  ToolHandle(const std::string& toolTypeAndName,
             const IInterface* parent = nullptr, bool createIf = true )
    : BaseToolHandle(parent,createIf),
      GaudiHandle<T>( toolTypeAndName,
                      toolComponentType(parent),
                      toolParentName(parent) ),
      m_pToolSvc( "ToolSvc", GaudiHandleBase::parentName() )
  {  }

public:

  StatusCode initialize(const std::string& toolTypeAndName,
                        const IInterface* parent = nullptr, bool createIf = true){

    GaudiHandleBase::setTypeAndName(toolTypeAndName);
    GaudiHandleBase::setComponentType(toolComponentType(parent));
    GaudiHandleBase::setParentName(toolParentName(parent));

    m_parent = parent;
    m_createIf = createIf;

    StatusCode sc = m_pToolSvc.initialize("ToolSvc", GaudiHandleBase::parentName());

    return sc;
  }

  /** Retrieve the AlgTool. Release existing tool if needed.
      Function must be repeated here to avoid hiding the function retrieve( T*& ) */
  StatusCode retrieve() const { // not really const, because it updates m_pObject
    return GaudiHandle<T>::retrieve();
  }

  /** Release the AlgTool.
      Function must be repeated here to avoid hiding the function release( T*& ) */
  StatusCode release() const { // not really const, because it updates m_pObject
    return GaudiHandle<T>::release();
  }

  /** Do the real retrieval of the AlgTool. */
  StatusCode retrieve( T*& algTool ) const override {
    IAlgTool* iface = nullptr;
    algTool = i_retrieve(iface) ? dynamic_cast<T*>(iface) : nullptr;
    return algTool ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }

  /** Do the real release of the AlgTool. */
  StatusCode release( T* algTool ) const override {
    return m_pToolSvc->releaseTool( algTool );
  }

  IAlgTool *get() const override {
    return GaudiHandle<T>::get();
  }
  std::string typeAndName() const override {
    return GaudiHandleBase::typeAndName();
  }

protected:
  StatusCode i_retrieve(IAlgTool*& algTool) const override {
    return m_pToolSvc->retrieve( typeAndName(), IAlgTool::interfaceID(),
                                 algTool,
                                 ToolHandleInfo::parent(), ToolHandleInfo::createIf() );
  }

private:
  //
  // Private data members
  //
  mutable ServiceHandle<IToolSvc> m_pToolSvc;
};

//-------------------------------------------------------------------------//



/** @class ToolHandleArray ToolHandle.h GaudiKernel/ToolHandle.h

    Array of Handles to be used in lieu of vector of naked pointers to tools.
    This allows better control through the framework of tool loading and usage.
    @parameter T is the AlgTool interface class (or concrete class) of
    the tool to use, and must derive from IAlgTool.

    @author Martin.Woudstra@cern.ch
*/

template < class T >
class ToolHandleArray : public ToolHandleInfo, public GaudiHandleArray< ToolHandle<T> > {
public:
  //
  // Constructors
  //
  /** Generic constructor. Probably not very useful...
      @param typesAndNamesList : a vector of strings with the concrete "type/name" strings
      for the list of tools
      @param parent   : passed on to ToolHandle, so has the same meaning as for ToolHandle
      @param createIf : passed on to ToolHandle, so has the same meaning as for ToolHandle
  */
  ToolHandleArray( const std::vector< std::string >& myTypesAndNames,
                   const IInterface* parent = 0, bool createIf = true )
    : ToolHandleInfo( parent, createIf ),
      GaudiHandleArray< ToolHandle<T> >( myTypesAndNames,
                                         ToolHandleInfo::toolComponentType(parent),
                                         ToolHandleInfo::toolParentName(parent) )
  {}

  /** Constructor which creates and empty list.
      @param parent   : passed on to ToolHandle, so has the same meaning as for ToolHandle
      @param createIf : passed on to ToolHandle, so has the same meaning as for ToolHandle
  */
  ToolHandleArray( const IInterface* parent = 0, bool createIf = true )
    : ToolHandleInfo( parent, createIf ),
      GaudiHandleArray< ToolHandle<T> >( ToolHandleInfo::toolComponentType(parent),
                                         ToolHandleInfo::toolParentName(parent) )
  { }

  /** Add a handle to the array with given tool type and name.
      This function overrides the one in GaudiHandleArray<T>, as this is a special case.
      The private/public choice and createIf is determined by what was given
      in the constructor of the ToolHandleArray. */
  virtual bool push_back( const std::string& toolTypeAndName ) {
    ToolHandle<T> handle( toolTypeAndName,
                          ToolHandleInfo::parent(),
                          ToolHandleInfo::createIf() );
    GaudiHandleArray< ToolHandle<T> >::push_back( handle );
    return true;
  }

  /** Ensure that for added handles the parent and creatIf are taken from this array. */
  virtual bool push_back( const ToolHandle<T>& myHandle ) {
    return push_back( myHandle.typeAndName() );
  }

};


template <class T>
inline std::ostream& operator<<( std::ostream& os, const ToolHandle<T>& handle ) {
  return operator<<(os, static_cast<const GaudiHandleInfo&>(handle) );
}


template <class T>
inline std::ostream& operator<<( std::ostream& os, const ToolHandleArray<T>& handle ) {
  return operator<<(os, static_cast<const GaudiHandleInfo&>(handle) );
}


#endif // ! GAUDIKERNEL_TOOLHANDLE_H
